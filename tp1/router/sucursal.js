const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Sucursal.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Sucursal.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
  
    let cantidad_vehiculos = req.body.modelo ;
    let numero_sucursal =  req.body.numero_sucursal ;
      
       }).then( =>{
        models.Sucursal.create({
            cantidad_vehiculos: cantidad_vehiculos,
            numero_sucursal : numero_sucursal
            
        }).then(() => {
            res.sendStatus(201);
        })
       })
    
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let cantidad_vehiculos = req.body.cantidad_vehiculos;
    let numero_sucursal =  req.body.numero_sucursal ;

    models.Sucursal.update(
        {
           
            
             cantidad_vehiculos = req.body.cantidad_vehiculos ,
             numero_sucursal =  req.body.numero_sucursal ,
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Sucursal.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;
