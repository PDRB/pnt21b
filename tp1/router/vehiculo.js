const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Vehiculo.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Vehiculo.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {

    let marca = req.body.marca ;
    let modelo = req.body.modelo ;
    let numero_sucursal =  req.body.numero_sucursal ;
      
       models.Sucursal.findOne({
           where :{
               numero_sucursal : numero_sucursal
           }
       }).then((Sucursal) =>{
        models.Vehiculo.create({
            marca: marca,
            modelo : modelo,
            numero_sucursal : numero_sucursal
            
        }).then(() => {
            res.sendStatus(201);
        })
       })
    
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let marca = req.body.marca ;
    let modelo = req.body.modelo ;
    let numero_sucursal =  req.body.numero_sucursal ;

    models.Vehiculo.update(
        {
           
             marca = req.body.marca ,
             modelo = req.body.modelo ,
             numero_sucursal =  req.body.numero_sucursal 
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Vehiculo.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;
