const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Empleado.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Empleado.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
   
    let nombre = req.body.nombre ;
    let apellido = req.body.apellido;
    let cargo =  req.body.cargo;
    let numero_sucursal =  req.body.numero_sucursal;
       models.Sucursal.findOne({
           where :{
               numero_sucursal : numero_sucursal
           }
       }).then((Sucursal ) =>{
        models.Empleado.create({
            nombre: nombre,
            apellido : apellido,
            cargo : cargo,
            numero_sucursal : numero_sucursal
            
        }).then(() => {
            res.sendStatus(201);
        })
       })
    
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let nombre = req.body.nombre ;
    let apellido = req.body.apellido;
    let cargo =  req.body.cargo;
    let numero_sucursal =  req.body.numero_sucursal;

    models.Empleado.update(
        {
            nombre: nombre,
            apellido : apellido,
            cargo : cargo,
            numero_sucursal : numero_sucursal
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Empleado.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;
