const Sequelize = require('sequelize');
const Sucursal = require('./Sucursal');
const Vehiculo = require('./Vehiculo');
const Empleado = require('./Empleado');


let db = {};

const sequelize = new Sequelize('mainDb', null, null, {
    dialect: "sqlite",
    storage: "./mainDB.sqlite"
});

// const sequelize = new Sequelize('mainDb', null, null, {
//     dialect: "mssql",
//     dialectModulePath: 'sequelize-msnodesqlv8',
//     dialectOptions: {
//         connectionString: 'Server=localhost/SQLSERVER2017;Database=master; Trusted_Connection=yes;'
//       },
// });

sequelize.authenticate()
    .then(function () { console.log('Autenticado'); })
    .catch(function () { console.log('Error autenticando'); })

db.Sucursal = Sucursal(sequelize, Sequelize);
db.Vehiculo = Vehiculo(sequelize, Sequelize);
db.Empleado = Empleado(sequelize, Sequelize);

db.Cliente.associate(db);

// sequelize.sync({ force: true })
//     .then(function (err) {
//         console.log('Modelo sincronizado.');
//     });

module.exports = db;
