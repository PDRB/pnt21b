module.exports = (sequelize, Sequelize) => {
    let Empleado =sequelize.define('Empleado',{
        nombre : Sequelize.STRING ,
        apellido : Sequelize.STRING,
        cargo : Sequelize.STRING,
        numero_sucursal : Sequelize.INTEGER
    });
    Empleado.associate = (models) => {
        models.Empleado.belongsTo(models.numero_sucursal, {
            onDelete: "CASCADE",
            foreingKey: {
                allowNull: false
            }
        });
    }


return Empleado
}
