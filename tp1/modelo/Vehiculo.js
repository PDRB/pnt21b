module.exports = (sequelize, Sequelize) => {
    let Vehiculo =sequelize.define('Vehiculo',{
        marca: Sequelize.STRING ,
        modelo: Sequelize.STRING ,
        numero_sucursal : Sequelize.INTEGER
    });
    Vehiculo.associate = (models) => {
        models.Vehiculo.belongsTo(models.Sucursal, {
            onDelete: "CASCADE",
            foreingKey: {
                allowNull: false
            }
        });
    }


return Vehiculo
}