const express = require('express');
const bodyParser = require('body-parser');

const routerEmpleado = require('./routes/empleado');
const routerSucursal = require('./routes/sucursal');
const routerVehiculo = require('./routes/vehiculo');
const routerConcesionario = require('./routes/concesionario');

const db = require('./models');

const app = express();

app.use(bodyParser.json());
app.use('/sucursal', routerSucursal);
app.use('/empleado', routerEmpleado);
app.use('/vehiculo', routerVehiculo);
app.use('/concesionario', routerConcesionario);


app.listen(port=5000, () => {
    console.log('Servidor escuchando...');
});
